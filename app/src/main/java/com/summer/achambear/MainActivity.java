package com.summer.achambear;

import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;

import android.os.Bundle;
import android.util.Log;
import android.widget.Toast;

import com.summer.achambear.databinding.ActivityMainBinding;

public class MainActivity extends AppCompatActivity {


    ActivityMainBinding binding;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        binding = ActivityMainBinding.inflate((getLayoutInflater()));
        setContentView(binding.getRoot());
        replaceFragment(new HomeFragment());

        binding.bottomNavigationView.setOnItemSelectedListener(item -> {
            switch (item.getItemId()){
                case R.id.search:
                    Log.d("emprendimiento","search");
                    replaceFragment(new SearchFragment());
                    break;
                case R.id.home:
                    Log.d("emprendimiento","home");
                    replaceFragment(new HomeFragment());
                    break;
                case R.id.profile:
                    Log.d("emprendimiento","profile");
                    replaceFragment(new ProfileFragment());
                    break;

            }
            return true;
        });
        binding.bottomNavigationView.setOnItemReselectedListener(item -> {
            switch (item.getItemId()){
                case R.id.search:
                    Log.d("emprendimiento","again search");
                    Toast.makeText(getApplicationContext(),"This item" + R.id.search + "is alrey selected",Toast.LENGTH_SHORT);
                    break;
                case R.id.home:
                    Log.d("emprendimiento","again home");
                    Toast.makeText(getApplicationContext(),"This item" + R.id.home + "is alrey selected",Toast.LENGTH_SHORT);
                    break;
                case R.id.profile:
                    Log.d("emprendimiento","again profile");
                    Toast.makeText(getApplicationContext(),"This item" + R.id.profile + "is alrey selected",Toast.LENGTH_SHORT);
                    break;

            }
        });
    }
    private void replaceFragment(Fragment fragment){
        FragmentManager fragmentManager= getSupportFragmentManager();
        FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
        fragmentTransaction.replace(R.id.fragment_layout, fragment);
        fragmentTransaction.commit();
    }
}